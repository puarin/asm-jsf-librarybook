/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm.da;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Puarinnnn
 */
public class ManagerAccess {

    public static boolean validate(String user, String pwd) {
        Connection con = null;
        PreparedStatement ps = null;
        try {
			con = DBConnection.getConnection();
			ps = con.prepareStatement("Select username, password from Users where username = ? and password = ?");
			ps.setString(1, user);
			ps.setString(2, pwd);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				//result found, means valid inputs
				return true;
			}
		} catch (SQLException ex) {
			System.out.println("Login error -->" + ex.getMessage());
			return false;
		} finally {
			DBConnection.close(con);
		}
		return false;
	}
    }
    
