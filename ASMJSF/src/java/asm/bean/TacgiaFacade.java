/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm.bean;

import asm.entity.Tacgia;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Puarinnnn
 */
@Stateless
public class TacgiaFacade extends AbstractFacade<Tacgia> {

    @PersistenceContext(unitName = "ASMJSFPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TacgiaFacade() {
        super(Tacgia.class);
    }
    
}
