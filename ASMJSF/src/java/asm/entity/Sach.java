/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Puarinnnn
 */
@Entity
@Table(name = "Sach")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sach.findAll", query = "SELECT s FROM Sach s")
    , @NamedQuery(name = "Sach.findByMaSach", query = "SELECT s FROM Sach s WHERE s.maSach = :maSach")
    , @NamedQuery(name = "Sach.findByMaLoai", query = "SELECT s FROM Sach s WHERE s.maLoai = :maLoai")
    , @NamedQuery(name = "Sach.findByTensach", query = "SELECT s FROM Sach s WHERE s.tensach = :tensach")
    , @NamedQuery(name = "Sach.findByTomtat", query = "SELECT s FROM Sach s WHERE s.tomtat = :tomtat")})
public class Sach implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaSach")
    private Integer maSach;
    @Size(max = 5)
    @Column(name = "MaLoai")
    private String maLoai;
    @Size(max = 50)
    @Column(name = "Tensach")
    private String tensach;
    @Size(max = 500)
    @Column(name = "Tomtat")
    private String tomtat;
    @JoinColumn(name = "MaNXB", referencedColumnName = "MaNXB")
    @ManyToOne(fetch = FetchType.EAGER)
    private NhaXB maNXB;
    @JoinColumn(name = "MaTG", referencedColumnName = "MaTG")
    @ManyToOne(fetch = FetchType.EAGER)
    private Tacgia maTG;

    public Sach() {
    }

    public Sach(Integer maSach) {
        this.maSach = maSach;
    }

    public Integer getMaSach() {
        return maSach;
    }

    public void setMaSach(Integer maSach) {
        this.maSach = maSach;
    }

    public String getMaLoai() {
        return maLoai;
    }

    public void setMaLoai(String maLoai) {
        this.maLoai = maLoai;
    }

    public String getTensach() {
        return tensach;
    }

    public void setTensach(String tensach) {
        this.tensach = tensach;
    }

    public String getTomtat() {
        return tomtat;
    }

    public void setTomtat(String tomtat) {
        this.tomtat = tomtat;
    }

    public NhaXB getMaNXB() {
        return maNXB;
    }

    public void setMaNXB(NhaXB maNXB) {
        this.maNXB = maNXB;
    }

    public Tacgia getMaTG() {
        return maTG;
    }

    public void setMaTG(Tacgia maTG) {
        this.maTG = maTG;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maSach != null ? maSach.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sach)) {
            return false;
        }
        Sach other = (Sach) object;
        if ((this.maSach == null && other.maSach != null) || (this.maSach != null && !this.maSach.equals(other.maSach))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "asm.entity.Sach[ maSach=" + maSach + " ]";
    }
    
}
